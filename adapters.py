#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from jsonpickle import decode

from resources import DATABASE_JSON_KEY, COLLECTION_FIELD_DELIMITER, SENSOR_JSON_KEY
from configfile import Database as repositoryDatabase
from configfile import Sensor as repositorySensor

def databaseUItoRepoFormat(databaseUI, sensorUI):
    repoDataBase = repositoryDatabase()
    repoDataBase.setAddress(databaseUI.getAddress())
    repoDataBase.setName(databaseUI.getName())
    collection = databaseUI.getPatientName() + COLLECTION_FIELD_DELIMITER + \
                 databaseUI.getAgeYears() + COLLECTION_FIELD_DELIMITER + \
                 databaseUI.getAgeMonths() + COLLECTION_FIELD_DELIMITER + \
                 databaseUI.getGender() + COLLECTION_FIELD_DELIMITER + \
                 sensorUI.getSide()
    repoDataBase.setCollection(collection)
    return repoDataBase

def sensorUItoRepoFormat(sensorUI):
    sensor = repositorySensor()
    sensor.setPosition(sensorUI.getSide())
    sensor.setName(sensorUI.getName())
    uiAddress = sensorUI.getAddress()
    sensorAddress = list(map(lambda v: int(v), uiAddress))
    sensor.setAddress(sensorAddress)
    sensor.setTimeout(int(sensorUI.getTimeout()))
    return sensor

def stringToRepoDatabase(source):
    configurationData = decode(source)
    databaseJson = configurationData.get(DATABASE_JSON_KEY)
    if databaseJson is None:
        return None
    repoDatabase = repositoryDatabase()
    repoDatabase.__dict__ = databaseJson
    return repoDatabase

def repoDataBaseToDatabaseUI(repoDatabase, databaseUI):
    databaseUI.setAddress(repoDatabase.getAddress())
    databaseUI.setName(repoDatabase.getName())
    collection = repoDatabase.getCollection()
    components = collection.split('_')
    _side = components.pop()
    databaseUI.setGender(components.pop())
    databaseUI.setAgeMonths(components.pop())
    databaseUI.setAgeYears(components.pop())
    databaseUI.setPatientName(components.pop())

def stringToRepoSensor(source):
    configurationData = decode(source)
    sensorJson = configurationData.get(SENSOR_JSON_KEY)
    if sensorJson is None:
        return None
    repoSensor = repositorySensor()
    repoSensor.__dict__ = sensorJson
    return repoSensor

def repoSensorToSensorUI(repoSensor, sensorUI):
    sensorUI.setName(repoSensor.getName())
    sensorUI.setTimeout(repoSensor.getTimeout())
    sensorUI.setAddress(repoSensor.getAddress())

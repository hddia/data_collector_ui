#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from tkinter import Frame, LEFT, BOTH, Tk
from os.path import isfile

from padding import Padding
from sensorUIcontainer import Sensor as sensorUIobject
from databaseUIcontainer import Database as databaseUIobject
from adapters import databaseUItoRepoFormat, sensorUItoRepoFormat, stringToRepoDatabase, repoDataBaseToDatabaseUI, repoSensorToSensorUI, stringToRepoSensor
from configfile import ConfigFile
from statusUIcontainer import StatusBox
from backendControllers import DataCollectionHandler
from buttons import Buttons
from resources import LEFT_SENSOR_DATA_FILE, RIGHT_SENSOR_DATA_FILE, LEFT_SENSOR_LABEL, RIGHT_SENSOR_LABEL, BACKGROUND_COLOR
from license import LicenseInfo

class Gui():
    def __init__(self):
        self.window = self.getMainWindow()
        self.dataCollector = None

    def addAllElements(self):
        self.window.protocol('WM_DELETE_WINDOW', self.onClose)

        padding = Padding()
        parentContainer = Frame(self.window, bg=BACKGROUND_COLOR)

        self.databaseUI = databaseUIobject(parentContainer, padding)
        self.databaseUI.pack(padding)

        self.leftSensorUI = sensorUIobject(parentContainer, LEFT_SENSOR_LABEL, padding)
        self.leftSensorUI.pack(padding)

        self.rightSensorUI = sensorUIobject(parentContainer, RIGHT_SENSOR_LABEL, padding)
        self.rightSensorUI.pack(padding)

        self.status = StatusBox(self.window)
        self.setDoNotShowCopyrightInformation()

        self.dataCollector = DataCollectionHandler()

        self.buttons = Buttons(parentContainer, self.save, self.start, self.stop)
        self.buttons.pack(padding)

        licenseInfo = LicenseInfo(self.window)
        licenseInfo.pack(padding)

        parentContainer.pack(side=LEFT, pady=(padding.mainContainerLeftPad, padding.mainContainerRightPad), padx=(padding.mainContainerLeftPad, 0), anchor='nw', fill=BOTH)

        self.status.pack(padding)

        self.load()

    def show(self):
        self.window.mainloop()

    def getMainWindow(self):
        window = Tk()
        window.config(background=BACKGROUND_COLOR)
        window.title('DHD Data collector UI')
        return window

    def areInputFieldsValid(self, database, leftSensor, rightSensor):
        error = database.validateInputFields()
        if error is not None:
            return error

        error = leftSensor.validateInputFields()
        if error is not None:
            return error

        error = rightSensor.validateInputFields()
        if error is not None:
            return error

        return None

    def save(self):
        self.buttons.setInProcessingState()

        error = self.areInputFieldsValid(self.databaseUI, self.leftSensorUI, self.rightSensorUI)
        if error is not None:
            self.status.showMessage(error)
            self.buttons.setInInitialState()
            return
        self.status.showMessage('Input fields are valid')

        leftRepoDataBase = databaseUItoRepoFormat(self.databaseUI, self.leftSensorUI)
        leftSensor = sensorUItoRepoFormat(self.leftSensorUI)
        ConfigFile(leftRepoDataBase, leftSensor).save(LEFT_SENSOR_DATA_FILE)

        rightRepoDataBase = databaseUItoRepoFormat(self.databaseUI, self.rightSensorUI)
        rightSensor = sensorUItoRepoFormat(self.rightSensorUI)
        ConfigFile(rightRepoDataBase, rightSensor).save(RIGHT_SENSOR_DATA_FILE)

        self.buttons.setInInitialState()

    def start(self):
        self.buttons.setInProcessingState()

        self.dataCollector.run(self.status.showFilteredMesage)
        if not self.dataCollector.isRunning():
            self.stop()
            self.buttons.setInInitialState()
        else:
            self.buttons.setInDataAquisitionRunningState()

    def stop(self):
        self.buttons.setInProcessingState()
        self.dataCollector.end()
        self.buttons.setInInitialState()

    def onClose(self):
        if self.dataCollector is not None:
            self.dataCollector.end()
        self.window.destroy()

    def load(self):
        if isfile(LEFT_SENSOR_DATA_FILE):
            with open(LEFT_SENSOR_DATA_FILE, 'r') as source:
                sensorData = source.read()
            repoDataBaseToDatabaseUI (stringToRepoDatabase(sensorData), self.databaseUI)
            repoSensorToSensorUI ( stringToRepoSensor(sensorData), self.leftSensorUI)

        if isfile(RIGHT_SENSOR_DATA_FILE):
            with open(RIGHT_SENSOR_DATA_FILE, 'r') as source:
                sensorData = source.read()
            repoSensorToSensorUI ( stringToRepoSensor(sensorData), self.rightSensorUI)

    def setDoNotShowCopyrightInformation(self):
        return self.status.addFilteredText('Copyright')

gui = Gui()
gui.addAllElements()
gui.show()
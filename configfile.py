#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from jsonpickle import encode

class Database:
    def __init__(self):
        self.uri = None
        self.name = None
        self.collection = None

    def setAddress(self, value):
        self.uri = value

    def getAddress(self):
        return self.uri

    def setName(self, value):
        self.name = value

    def getName(self):
        return self.name

    def setCollection(self, collection):
        self.collection = collection

    def getCollection(self):
        return self.collection

class Sensor:
    def __init__(self):
        self.position = None
        self.name = None
        self.address = []
        self.timeout = None

    def setPosition(self, value):
        self.position = value

    def getPosition(self):
        return self.position

    def setName(self, value):
        self.name = value

    def getName(self):
        return self.name

    def setAddress(self, address):
        self.address = address

    def getAddress(self):
        return self.address

    def setTimeout(self, value):
        self.timeout = value

    def getTimeout(self):
        return self.timeout

class ConfigFile:
    def __init__(self, databaseInfo, sensorInfo):
        self.database = databaseInfo
        self.sensor = sensorInfo

    def toJsonString(self):
        return encode(self, unpicklable=False)

    def save(self, fileName):
        with open(fileName,"w+") as output:
            output.write(self.toJsonString())

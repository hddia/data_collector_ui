#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from tkinter import Frame, StringVar, Label, Entry, TRUE, X

from resources import BACKGROUND_COLOR
from validators import removeNonNumericCharacters, removeInvalidByteValue, getNumber

class Sensor():
    def __init__(self, parent, side, padding):
        self.frame = Frame(parent, bg=BACKGROUND_COLOR)
        self.frame.grid_columnconfigure(1, weight=1)
        self.frame.grid_columnconfigure(3, weight=1)
        self.frame.grid_columnconfigure(5, weight=1)
        self.frame.grid_columnconfigure(7, weight=1)
        self.frame.grid_columnconfigure(9, weight=1)
        self.frame.grid_columnconfigure(11, weight=1)

        self.addSideLabel(side, padding.fieldYpadding)
        self.name = StringVar()
        self.addEntryBox(self.frame, 'Name: ', self.name, padding.fieldYpadding, 1, 0)
        self.addNetworkAddressEntryBox(padding.fieldYpadding)
        self.timeout = StringVar()
        self.timeout.trace('w', lambda *_, var=self.timeout: removeNonNumericCharacters(var, 10))
        self.addEntryBox(self.frame, 'Timeout: ', self.timeout, padding.fieldYpadding, 3, 0)

    def addNetworkAddressEntryBox(self, yPadding):
        addressLabel = Label(self.frame, text='Network address: ', bg=BACKGROUND_COLOR)
        addressLabel.grid(row=2, column=0, sticky='w', pady=yPadding)

        entryWidth = 7

        self.addressByte0 = StringVar()
        self.addressByte0.trace('w', lambda *_, var=self.addressByte0: removeInvalidByteValue(var))
        byte0Entry = Entry(self.frame, justify='center', textvariable=self.addressByte0, width=entryWidth, bg=BACKGROUND_COLOR)
        byte0Entry.grid(row=2, column=1, sticky='we', pady=yPadding)
        byte0SeparatorLabel = Label(self.frame, text=':', bg=BACKGROUND_COLOR)
        byte0SeparatorLabel.grid(row=2, column=2, sticky='w', pady=yPadding)

        self.addressByte1 = StringVar()
        self.addressByte1.trace('w', lambda *_, var=self.addressByte1: removeInvalidByteValue(var))
        byte1Entry = Entry(self.frame, justify='center', textvariable=self.addressByte1, width=entryWidth, bg=BACKGROUND_COLOR)
        byte1Entry.grid(row=2, column=3, sticky='we', pady=yPadding)
        byte1SeparatorLabel = Label(self.frame, text=':', bg=BACKGROUND_COLOR)
        byte1SeparatorLabel.grid(row=2, column=4, sticky='w', pady=yPadding)

        self.addressByte2 = StringVar()
        self.addressByte2.trace('w', lambda *_, var=self.addressByte2: removeInvalidByteValue(var))
        byte2Entry = Entry(self.frame, justify='center', textvariable=self.addressByte2, width=entryWidth, bg=BACKGROUND_COLOR)
        byte2Entry.grid(row=2, column=5, sticky='we', pady=yPadding)
        byte2SeparatorLabel = Label(self.frame, text=':', bg=BACKGROUND_COLOR)
        byte2SeparatorLabel.grid(row=2, column=6, sticky='w', pady=yPadding)

        self.addressByte3 = StringVar()
        self.addressByte3.trace('w', lambda *_, var=self.addressByte3: removeInvalidByteValue(var))
        byte3Entry = Entry(self.frame, justify='center', textvariable=self.addressByte3, width=entryWidth, bg=BACKGROUND_COLOR)
        byte3Entry.grid(row=2, column=7, sticky='we', pady=yPadding)
        byte3SeparatorLabel = Label(self.frame, text=':', bg=BACKGROUND_COLOR)
        byte3SeparatorLabel.grid(row=2, column=8, sticky='w', pady=yPadding)

        self.addressByte4 = StringVar()
        self.addressByte4.trace('w', lambda *_, var=self.addressByte4: removeInvalidByteValue(var))
        byte4Entry = Entry(self.frame, justify='center', textvariable=self.addressByte4, width=entryWidth, bg=BACKGROUND_COLOR)
        byte4Entry.grid(row=2, column=9, sticky='we', pady=yPadding)
        byte4SeparatorLabel = Label(self.frame, text=':', bg=BACKGROUND_COLOR)
        byte4SeparatorLabel.grid(row=2, column=10, sticky='w', pady=yPadding)

        self.addressByte5 = StringVar()
        self.addressByte5.trace('w', lambda *_, var=self.addressByte5: removeInvalidByteValue(var))
        byte5Entry = Entry(self.frame, justify='center', textvariable=self.addressByte5, width=entryWidth, bg=BACKGROUND_COLOR)
        byte5Entry.grid(row=2, column=11, sticky='we', pady=yPadding)

    def addEntryBox(self, parent, label, var, yPadding, baseRow, baseCollumn):
        label = Label(parent, text=label, bg=BACKGROUND_COLOR)
        label.grid(row=baseRow, column=baseCollumn, sticky='w', pady=yPadding)
        entry = Entry(parent, textvariable=var, bg=BACKGROUND_COLOR)
        entry.grid(row=baseRow, column=baseCollumn+1, columnspan=11, sticky='we', pady=yPadding)

    def addSideLabel(self, side, yPadding):
        sideLabel = Label(self.frame, text='Sensor side: ', bg=BACKGROUND_COLOR)
        sideLabel.grid(row=0, column=0, sticky='w', pady=yPadding)
        self.side = StringVar()
        self.side.set(side)
        shownSide = Label(self.frame, textvariable=self.side, bg=BACKGROUND_COLOR)
        shownSide.grid(row=0, column=1, columnspan=11, sticky='w', pady=yPadding)

    def pack(self, padding):
        yPadding = (padding.containerLeftPadRatio*padding.fieldYpadding, padding.containerRightPadRation*padding.fieldYpadding)
        xPadding = (padding.containerLeftPadRatio*padding.fieldYpadding, padding.containerRightPadRation*padding.fieldYpadding)
        self.frame.pack(pady=yPadding, padx=xPadding, anchor='e', expand=TRUE, fill=X)

    def getSide(self):
        return self.side.get()

    def getName(self):
        return self.name.get()

    def setName(self, value):
        self.name.set(value)

    def getTimeout(self):
        return self.timeout.get()

    def setTimeout(self, value):
        if getNumber(value, 10) is not None:
            self.timeout.set(value)

    def getAddress(self):
        address = []
        if self.appendByteIfValid(self.addressByte0.get(), address) is False:
            return None
        if self.appendByteIfValid(self.addressByte1.get(), address) is False:
            return None
        if self.appendByteIfValid(self.addressByte2.get(), address) is False:
            return None
        if self.appendByteIfValid(self.addressByte3.get(), address) is False:
            return None
        if self.appendByteIfValid(self.addressByte4.get(), address) is False:
            return None
        if self.appendByteIfValid(self.addressByte5.get(), address) is False:
            return None
        return address

    def appendByteIfValid(self, byte, address):
        if byte is not '':
            address.append(byte)
            return True
        return False

    def setAddress(self, valueArray):
        self.setIfValidNumber(self.addressByte5, valueArray.pop())
        self.setIfValidNumber(self.addressByte4, valueArray.pop())
        self.setIfValidNumber(self.addressByte3, valueArray.pop())
        self.setIfValidNumber(self.addressByte2, valueArray.pop())
        self.setIfValidNumber(self.addressByte1, valueArray.pop())
        self.setIfValidNumber(self.addressByte0, valueArray.pop())

    def setIfValidNumber(self, variable, value):
        asNumber = getNumber(value, 10)
        if asNumber is not None:
            variable.set(asNumber)

    def validateInputFields(self):
        side = self.getSide()
        if side is '':
            return 'Invalid sensor side!'

        name = self.getName()
        if name is '':
            return 'Invalid '+side+' sensor name!'

        if self.getAddress() is None:
            return 'Invalid ' + side + ' sensor address!'

        timeout = self.getTimeout()
        if timeout is '':
            return 'Invalid '+side+' sensor timeout!'

#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from tkinter import RIGHT, Text,END, NW, BOTH

from resources import BACKGROUND_COLOR, MAX_NUMBER_OF_MESSAGES

class StatusBox():
    def __init__(self, parent):
        self.status = Text(parent, bg=BACKGROUND_COLOR, width=70)
        self.lineCount = 0
        self.filters = []

    def pack(self, padding):
        self.status.pack(side=RIGHT, pady=(padding.mainContainerLeftPad, 0), padx=(0, padding.mainContainerRightPad), anchor=NW, fill=BOTH, expand=True)

    def showMessage(self, message):
        line = message + '\n'
        self.status.insert(END, line)
        try:
            lastIndex = self.status.index(END)
            if (float(lastIndex) > MAX_NUMBER_OF_MESSAGES):
                self.status.delete( 0.0, MAX_NUMBER_OF_MESSAGES/2 )
        except ValueError:
            pass

        self.status.see(END)

    def addFilteredText(self, item):
        self.filters.append(item)

    def showFilteredMesage(self, message):
        for item in self.filters:
            if message.startswith(item):
                return
        self.showMessage(message)
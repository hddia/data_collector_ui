from tkinter import Label, BOTTOM

from resources import BACKGROUND_COLOR

class LicenseInfo():
    def __init__(self, parent):
        self.container = Label(parent, bg=BACKGROUND_COLOR, text="Copyright 2020 Bota Viorel. Distributed under the Apache License, Version 2.0")

    def pack(self, padding):
        yPadding = (0, padding.containerRightPadRation*padding.fieldYpadding)
        xPadding = (padding.containerLeftPadRatio*padding.fieldYpadding, padding.containerRightPadRation*padding.fieldYpadding)
        self.container.pack(side=BOTTOM,  pady=yPadding, padx=xPadding, anchor='se')

#    Copyright 2020 Bota Viorel
#
#    Licensed under the Apache License, Version 2.0 (the "License");
#    you may not use this file except in compliance with the License.
#    You may obtain a copy of the License at
#
#        http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS,
#    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#    See the License for the specific language governing permissions and
#    limitations under the License.

from resources import MAX_AGE_YEARS, genderOptions

def removeNonNumericCharacters(var, base):
    value = var.get()
    newLen = len(value) - 1
    if getNumber(value, base) is not None:
        return False
    var.set(value[:newLen])
    return True

def getNumber(value, base):
    try:
        number = int(str(value), base)
    except ValueError:
        return None
    return number

def removeInvalidMonthCharacter(var):
    if removeNonNumericCharacters(var, 10):
        return

    value = var.get()
    newLen = len(value) - 1
    if not isValidMonthValue(value):
        var.set(value[:newLen])

def isValidMonthValue(value):
    months = getNumber(value, 10)
    if months is None:
        return False

    if 0 > months or months > 12:
        return False
    return True

def removeInvalidYearsCharacter(var):
    if removeNonNumericCharacters(var, 10):
        return

    value = var.get()
    newLen = len(value) - 1
    if not isValidAgeYears(value):
        var.set(value[:newLen])

def isValidAgeYears(value):
    years = getNumber(value, 10)
    if years is None:
        return False

    if 0 > years or years > MAX_AGE_YEARS:
        return False

    return True

def removeInvalidGenderOptions(var):
    value = var.get()
    if not isValidGenderOption(value):
        var.set('')

def isValidGenderOption(value):
    return value is genderOptions[0] or value is genderOptions[1]

def removeInvalidByteValue(var):
    removeNonNumericCharacters(var, 10)
    value = var.get()
    if 0 == len(value):
        return
    newLen = len(value) - 1
    if int(value, 10) > 255:
        var.set(value[:newLen])